#include <string.h>

void i18n_short_month(char *lang, int month, char *dest) {
	if (strcmp(lang, "da") == 0) {
		if (month == 0) { strcpy(dest, "Jan"); }
		else if (month == 1) { strcpy(dest, "Feb"); }
		else if (month == 2) { strcpy(dest, "Mar"); }
		else if (month == 3) { strcpy(dest, "Apr"); }
		else if (month == 4) { strcpy(dest, "Maj"); }
		else if (month == 5) { strcpy(dest, "Jun"); }
		else if (month == 6) { strcpy(dest, "Jul"); }
		else if (month == 7) { strcpy(dest, "Aug"); }
		else if (month == 8) { strcpy(dest, "Sep"); }
		else if (month == 9) { strcpy(dest, "Okt"); }
		else if (month == 10) { strcpy(dest, "Nov"); }
		else if (month == 11) { strcpy(dest, "Dec"); }
	}
	else if (strcmp(lang, "de") == 0) {
		if (month == 0) { strcpy(dest, "Jan"); }
		else if (month == 1) { strcpy(dest, "Feb"); }
		else if (month == 2) { strcpy(dest, "Mär"); }
		else if (month == 3) { strcpy(dest, "Apr"); }
		else if (month == 4) { strcpy(dest, "Mai"); }
		else if (month == 5) { strcpy(dest, "Jun"); }
		else if (month == 6) { strcpy(dest, "ul"); }
		else if (month == 7) { strcpy(dest, "Aug"); }
		else if (month == 8) { strcpy(dest, "Sep"); }
		else if (month == 9) { strcpy(dest, "Okt"); }
		else if (month == 10) { strcpy(dest, "Nov"); }
		else if (month == 11) { strcpy(dest, "Dez"); }
	}
	else if (strcmp(lang, "es") == 0) {
		if (month == 0) { strcpy(dest, "Ene"); }
		else if (month == 1) { strcpy(dest, "Feb"); }
		else if (month == 2) { strcpy(dest, "Mar"); }
		else if (month == 3) { strcpy(dest, "Abr"); }
		else if (month == 4) { strcpy(dest, "May"); }
		else if (month == 5) { strcpy(dest, "Jun"); }
		else if (month == 6) { strcpy(dest, "Jul"); }
		else if (month == 7) { strcpy(dest, "Ago"); }
		else if (month == 8) { strcpy(dest, "Sep"); }
		else if (month == 9) { strcpy(dest, "Oct"); }
		else if (month == 10) { strcpy(dest, "Nov"); }
		else if (month == 11) { strcpy(dest, "Dec"); }
	}
	else if (strcmp(lang, "fr") == 0) {
		if (month == 0) { strcpy(dest, "Jan"); }
		else if (month == 1) { strcpy(dest, "Fév"); }
		else if (month == 2) { strcpy(dest, "Mar"); }
		else if (month == 3) { strcpy(dest, "Avr"); }
		else if (month == 4) { strcpy(dest, "Mai"); }
		else if (month == 5) { strcpy(dest, "Jun"); }
		else if (month == 6) { strcpy(dest, "Jui"); }
		else if (month == 7) { strcpy(dest, "Aoû"); }
		else if (month == 8) { strcpy(dest, "Sep"); }
		else if (month == 9) { strcpy(dest, "Oct"); }
		else if (month == 10) { strcpy(dest, "Nov"); }
		else if (month == 11) { strcpy(dest, "Déc"); }
	}
	else if (strcmp(lang, "hr") == 0) {
		if (month == 0) { strcpy(dest, "Sij"); }
		else if (month == 1) { strcpy(dest, "Vel"); }
		else if (month == 2) { strcpy(dest, "Ozu"); }
		else if (month == 3) { strcpy(dest, "Tra"); }
		else if (month == 4) { strcpy(dest, "Svi"); }
		else if (month == 5) { strcpy(dest, "Lip"); }
		else if (month == 6) { strcpy(dest, "Srp"); }
		else if (month == 7) { strcpy(dest, "Kol"); }
		else if (month == 8) { strcpy(dest, "Ruj"); }
		else if (month == 9) { strcpy(dest, "Lis"); }
		else if (month == 10) { strcpy(dest, "Stu"); }
		else if (month == 11) { strcpy(dest, "Pro"); }
	}
	else if (strcmp(lang, "it") == 0) {
		if (month == 0) { strcpy(dest, "Gen"); }
		else if (month == 1) { strcpy(dest, "Feb"); }
		else if (month == 2) { strcpy(dest, "Mar"); }
		else if (month == 3) { strcpy(dest, "Apr"); }
		else if (month == 4) { strcpy(dest, "Mag"); }
		else if (month == 5) { strcpy(dest, "Giu"); }
		else if (month == 6) { strcpy(dest, "Lug"); }
		else if (month == 7) { strcpy(dest, "Ago"); }
		else if (month == 8) { strcpy(dest, "Set"); }
		else if (month == 9) { strcpy(dest, "Ott"); }
		else if (month == 10) { strcpy(dest, "Nov"); }
		else if (month == 11) { strcpy(dest, "Dic"); }
	}
	else if (strcmp(lang, "la") == 0) {
		if (month == 0) { strcpy(dest, "Ian"); }
		else if (month == 1) { strcpy(dest, "Feb"); }
		else if (month == 2) { strcpy(dest, "Mar"); }
		else if (month == 3) { strcpy(dest, "Apr"); }
		else if (month == 4) { strcpy(dest, "Mai"); }
		else if (month == 5) { strcpy(dest, "Jun"); }
		else if (month == 6) { strcpy(dest, "Jul"); }
		else if (month == 7) { strcpy(dest, "Aug"); }
		else if (month == 8) { strcpy(dest, "Se"); }
		else if (month == 9) { strcpy(dest, "Oct"); }
		else if (month == 10) { strcpy(dest, "Nov"); }
		else if (month == 11) { strcpy(dest, "Dec"); }
	}
	else if (strcmp(lang, "ms") == 0) {
		if (month == 0) { strcpy(dest, "Jan"); }
		else if (month == 1) { strcpy(dest, "eb"); }
		else if (month == 2) { strcpy(dest, "Mac"); }
		else if (month == 3) { strcpy(dest, "Ap"); }
		else if (month == 4) { strcpy(dest, "Mei"); }
		else if (month == 5) { strcpy(dest, "Jun"); }
		else if (month == 6) { strcpy(dest, "Jul"); }
		else if (month == 7) { strcpy(dest, "Ogs"); }
		else if (month == 8) { strcpy(dest, "Sep"); }
		else if (month == 9) { strcpy(dest, "Okt"); }
		else if (month == 10) { strcpy(dest, "Nov"); }
		else if (month == 11) { strcpy(dest, "Dis"); }
	}
	else if (strcmp(lang, "nl") == 0) {
		if (month == 0) { strcpy(dest, "Jan"); }
		else if (month == 1) { strcpy(dest, "Feb"); }
		else if (month == 2) { strcpy(dest, "Mrt"); }
		else if (month == 3) { strcpy(dest, "Apr"); }
		else if (month == 4) { strcpy(dest, "Mei"); }
		else if (month == 5) { strcpy(dest, "Jun"); }
		else if (month == 6) { strcpy(dest, "Jul"); }
		else if (month == 7) { strcpy(dest, "Aug"); }
		else if (month == 8) { strcpy(dest, "Sep"); }
		else if (month == 9) { strcpy(dest, "Okt"); }
		else if (month == 10) { strcpy(dest, "Nov"); }
		else if (month == 11) { strcpy(dest, "Dec"); }
	}
	else if (strcmp(lang, "ru") == 0) {
		if (month == 0) { strcpy(dest, "Янв"); }
		else if (month == 1) { strcpy(dest, "Фев"); }
		else if (month == 2) { strcpy(dest, "Мар"); }
		else if (month == 3) { strcpy(dest, "Апр"); }
		else if (month == 4) { strcpy(dest, "Май"); }
		else if (month == 5) { strcpy(dest, "Июн"); }
		else if (month == 6) { strcpy(dest, "Июл"); }
		else if (month == 7) { strcpy(dest, "Авг"); }
		else if (month == 8) { strcpy(dest, "Сен"); }
		else if (month == 9) { strcpy(dest, "Окт"); }
		else if (month == 10) { strcpy(dest, "Ноя"); }
		else if (month == 11) { strcpy(dest, "Дек"); }
	}
	else if (strcmp(lang, "sv") == 0) {
		if (month == 0) { strcpy(dest, "Jan"); }
		else if (month == 1) { strcpy(dest, "Feb"); }
		else if (month == 2) { strcpy(dest, "Mar"); }
		else if (month == 3) { strcpy(dest, "Apr"); }
		else if (month == 4) { strcpy(dest, "Maj"); }
		else if (month == 5) { strcpy(dest, "Jun"); }
		else if (month == 6) { strcpy(dest, "Jul"); }
		else if (month == 7) { strcpy(dest, "Aug"); }
		else if (month == 8) { strcpy(dest, "Sep"); }
		else if (month == 9) { strcpy(dest, "Okt"); }
		else if (month == 10) { strcpy(dest, "Nov"); }
		else if (month == 11) { strcpy(dest, "Dec"); }
	}
	else if (strcmp(lang, "tr") == 0) {
		if (month == 0) { strcpy(dest, "Oca"); }
		else if (month == 1) { strcpy(dest, "Sub"); }
		else if (month == 2) { strcpy(dest, "Mar"); }
		else if (month == 3) { strcpy(dest, "Nis"); }
		else if (month == 4) { strcpy(dest, "May"); }
		else if (month == 5) { strcpy(dest, "Haz"); }
		else if (month == 6) { strcpy(dest, "Tem"); }
		else if (month == 7) { strcpy(dest, "Agu"); }
		else if (month == 8) { strcpy(dest, "Eyl"); }
		else if (month == 9) { strcpy(dest, "Eki"); }
		else if (month == 10) { strcpy(dest, "Kas"); }
		else if (month == 11) { strcpy(dest, "Ara"); }
	}
	else {
		if (month == 0) { strcpy(dest, "Jan"); }
		else if (month == 1) { strcpy(dest, "Feb"); }
		else if (month == 2) { strcpy(dest, "Mar"); }
		else if (month == 3) { strcpy(dest, "Apr"); }
		else if (month == 4) { strcpy(dest, "May."); }
		else if (month == 5) { strcpy(dest, "Jun"); }
		else if (month == 6) { strcpy(dest, "Jul"); }
		else if (month == 7) { strcpy(dest, "Aug"); }
		else if (month == 8) { strcpy(dest, "Sep"); }
		else if (month == 9) { strcpy(dest, "Oct"); }
		else if (month == 10) { strcpy(dest, "Nov"); }
		else if (month == 11) { strcpy(dest, "Dec"); }
	}

}

void i18n_month(char *lang, int month, char *dest) {
	if (strcmp(lang, "da") == 0) {
		if (month == 0) { strcpy(dest, "Januar"); }
		else if (month == 1) { strcpy(dest, "Februar"); }
		else if (month == 2) { strcpy(dest, "Marts"); }
		else if (month == 3) { strcpy(dest, "April"); }
		else if (month == 4) { strcpy(dest, "Maj"); }
		else if (month == 5) { strcpy(dest, "Juni"); }
		else if (month == 6) { strcpy(dest, "Juli"); }
		else if (month == 7) { strcpy(dest, "August"); }
		else if (month == 8) { strcpy(dest, "September"); }
		else if (month == 9) { strcpy(dest, "Oktober"); }
		else if (month == 10) { strcpy(dest, "November"); }
		else if (month == 11) { strcpy(dest, "December"); }
	}
	else if (strcmp(lang, "de") == 0) {
		if (month == 0) { strcpy(dest, "Januar"); }
		else if (month == 1) { strcpy(dest, "Februar"); }
		else if (month == 2) { strcpy(dest, "März"); }
		else if (month == 3) { strcpy(dest, "April"); }
		else if (month == 4) { strcpy(dest, "Mai"); }
		else if (month == 5) { strcpy(dest, "Juni"); }
		else if (month == 6) { strcpy(dest, "Juli"); }
		else if (month == 7) { strcpy(dest, "August"); }
		else if (month == 8) { strcpy(dest, "September"); }
		else if (month == 9) { strcpy(dest, "Oktober"); }
		else if (month == 10) { strcpy(dest, "November"); }
		else if (month == 11) { strcpy(dest, "Dezember"); }
	}
	else if (strcmp(lang, "es") == 0) {
		if (month == 0) { strcpy(dest, "Enero"); }
		else if (month == 1) { strcpy(dest, "Febrero"); }
		else if (month == 2) { strcpy(dest, "Marzo"); }
		else if (month == 3) { strcpy(dest, "Abril"); }
		else if (month == 4) { strcpy(dest, "Mayo"); }
		else if (month == 5) { strcpy(dest, "Junio"); }
		else if (month == 6) { strcpy(dest, "Julio"); }
		else if (month == 7) { strcpy(dest, "Agosto"); }
		else if (month == 8) { strcpy(dest, "Septiembre"); }
		else if (month == 9) { strcpy(dest, "Octubre"); }
		else if (month == 10) { strcpy(dest, "Noviembre"); }
		else if (month == 11) { strcpy(dest, "Diciembre"); }
	}
	else if (strcmp(lang, "fr") == 0) {
		if (month == 0) { strcpy(dest, "Janvier"); }
		else if (month == 1) { strcpy(dest, "Février"); }
		else if (month == 2) { strcpy(dest, "Mars"); }
		else if (month == 3) { strcpy(dest, "Avril"); }
		else if (month == 4) { strcpy(dest, "Mai"); }
		else if (month == 5) { strcpy(dest, "Juin"); }
		else if (month == 6) { strcpy(dest, "Juillet"); }
		else if (month == 7) { strcpy(dest, "Août"); }
		else if (month == 8) { strcpy(dest, "Septembre"); }
		else if (month == 9) { strcpy(dest, "Octobre"); }
		else if (month == 10) { strcpy(dest, "Novembre"); }
		else if (month == 11) { strcpy(dest, "Décembre"); }
	}
	else if (strcmp(lang, "hr") == 0) {
		if (month == 0) { strcpy(dest, "Sijecanj"); }
		else if (month == 1) { strcpy(dest, "Veljaca"); }
		else if (month == 2) { strcpy(dest, "Ozujak"); }
		else if (month == 3) { strcpy(dest, "Travanj"); }
		else if (month == 4) { strcpy(dest, "Svibanj"); }
		else if (month == 5) { strcpy(dest, "Lipanj"); }
		else if (month == 6) { strcpy(dest, "Srpanj"); }
		else if (month == 7) { strcpy(dest, "Kolovoz"); }
		else if (month == 8) { strcpy(dest, "Rujan"); }
		else if (month == 9) { strcpy(dest, "Listopad"); }
		else if (month == 10) { strcpy(dest, "Studeni"); }
		else if (month == 11) { strcpy(dest, "Prosinac"); }
	}
	else if (strcmp(lang, "hu") == 0) {
		if (month == 0) { strcpy(dest, "Január"); }
		else if (month == 1) { strcpy(dest, "Február"); }
		else if (month == 2) { strcpy(dest, "Március"); }
		else if (month == 3) { strcpy(dest, "Április"); }
		else if (month == 4) { strcpy(dest, "Május"); }
		else if (month == 5) { strcpy(dest, "Június"); }
		else if (month == 6) { strcpy(dest, "Július"); }
		else if (month == 7) { strcpy(dest, "Augusztus"); }
		else if (month == 8) { strcpy(dest, "Szeptember"); }
		else if (month == 9) { strcpy(dest, "Október"); }
		else if (month == 10) { strcpy(dest, "November"); }
		else if (month == 11) { strcpy(dest, "December"); }
	}
	else if (strcmp(lang, "it") == 0) {
		if (month == 0) { strcpy(dest, "Gennaio"); }
		else if (month == 1) { strcpy(dest, "Febbraio"); }
		else if (month == 2) { strcpy(dest, "Marzo"); }
		else if (month == 3) { strcpy(dest, "Aprile"); }
		else if (month == 4) { strcpy(dest, "Maggio"); }
		else if (month == 5) { strcpy(dest, "Giugno"); }
		else if (month == 6) { strcpy(dest, "Luglio"); }
		else if (month == 7) { strcpy(dest, "Agosto"); }
		else if (month == 8) { strcpy(dest, "Settembre"); }
		else if (month == 9) { strcpy(dest, "Ottobre"); }
		else if (month == 10) { strcpy(dest, "Novembre"); }
		else if (month == 11) { strcpy(dest, "Dicembre"); }
	}
	else if (strcmp(lang, "la") == 0) {
		if (month == 0) { strcpy(dest, "Ianuarius"); }
		else if (month == 1) { strcpy(dest, "Februarius"); }
		else if (month == 2) { strcpy(dest, "Martius"); }
		else if (month == 3) { strcpy(dest, "Aprilis"); }
		else if (month == 4) { strcpy(dest, "Maius"); }
		else if (month == 5) { strcpy(dest, "Iunius"); }
		else if (month == 6) { strcpy(dest, "Iulius"); }
		else if (month == 7) { strcpy(dest, "Augustus"); }
		else if (month == 8) { strcpy(dest, "September"); }
		else if (month == 9) { strcpy(dest, "October"); }
		else if (month == 10) { strcpy(dest, "November"); }
		else if (month == 11) { strcpy(dest, "December"); }
	}
	else if (strcmp(lang, "ms") == 0) {
		if (month == 0) { strcpy(dest, "Januari"); }
		else if (month == 1) { strcpy(dest, "Febuari"); }
		else if (month == 2) { strcpy(dest, "Mac"); }
		else if (month == 3) { strcpy(dest, "April"); }
		else if (month == 4) { strcpy(dest, "Mei"); }
		else if (month == 5) { strcpy(dest, "Jun"); }
		else if (month == 6) { strcpy(dest, "Juli"); }
		else if (month == 7) { strcpy(dest, "Ogos"); }
		else if (month == 8) { strcpy(dest, "September"); }
		else if (month == 9) { strcpy(dest, "Oktober"); }
		else if (month == 10) { strcpy(dest, "November"); }
		else if (month == 11) { strcpy(dest, "Disember"); }
	}
	else if (strcmp(lang, "nl") == 0) {
		if (month == 0) { strcpy(dest, "Januari"); }
		else if (month == 1) { strcpy(dest, "Februari"); }
		else if (month == 2) { strcpy(dest, "Maart"); }
		else if (month == 3) { strcpy(dest, "April"); }
		else if (month == 4) { strcpy(dest, "Mei"); }
		else if (month == 5) { strcpy(dest, "Juni"); }
		else if (month == 6) { strcpy(dest, "Juli"); }
		else if (month == 7) { strcpy(dest, "Augustus"); }
		else if (month == 8) { strcpy(dest, "September"); }
		else if (month == 9) { strcpy(dest, "Oktober"); }
		else if (month == 10) { strcpy(dest, "November"); }
		else if (month == 11) { strcpy(dest, "December"); }
	}
	else if (strcmp(lang, "ru") == 0) {
		if (month == 0) { strcpy(dest, "Январь"); }
		else if (month == 1) { strcpy(dest, "Февраль"); }
		else if (month == 2) { strcpy(dest, "Март"); }
		else if (month == 3) { strcpy(dest, "Апрель"); }
		else if (month == 4) { strcpy(dest, "Май"); }
		else if (month == 5) { strcpy(dest, "Июнь"); }
		else if (month == 6) { strcpy(dest, "Июль"); }
		else if (month == 7) { strcpy(dest, "Август"); }
		else if (month == 8) { strcpy(dest, "Сентябрь"); }
		else if (month == 9) { strcpy(dest, "Октябрь"); }
		else if (month == 10) { strcpy(dest, "Ноябрь"); }
		else if (month == 11) { strcpy(dest, "Декабрь"); }
	}
	else if (strcmp(lang, "sv") == 0) {
		if (month == 0) { strcpy(dest, "Januari"); }
		else if (month == 1) { strcpy(dest, "Februari"); }
		else if (month == 2) { strcpy(dest, "Mars"); }
		else if (month == 3) { strcpy(dest, "April"); }
		else if (month == 4) { strcpy(dest, "Maj"); }
		else if (month == 5) { strcpy(dest, "Juni"); }
		else if (month == 6) { strcpy(dest, "Juli"); }
		else if (month == 7) { strcpy(dest, "Augusti"); }
		else if (month == 8) { strcpy(dest, "September"); }
		else if (month == 9) { strcpy(dest, "Oktober"); }
		else if (month == 10) { strcpy(dest, "November"); }
		else if (month == 11) { strcpy(dest, "December"); }
	}
	else if (strcmp(lang, "tr") == 0) {
		if (month == 0) { strcpy(dest, "Ocak"); }
		else if (month == 1) { strcpy(dest, "Şubat"); }
		else if (month == 2) { strcpy(dest, "Mart"); }
		else if (month == 3) { strcpy(dest, "Nisan"); }
		else if (month == 4) { strcpy(dest, "Mayıs"); }
		else if (month == 5) { strcpy(dest, "Haziran"); }
		else if (month == 6) { strcpy(dest, "Temmuz"); }
		else if (month == 7) { strcpy(dest, "Ağustos"); }
		else if (month == 8) { strcpy(dest, "Eylül"); }
		else if (month == 9) { strcpy(dest, "Ekim"); }
		else if (month == 10) { strcpy(dest, "Kasım"); }
		else if (month == 11) { strcpy(dest, "Aralık"); }
	}
	else {
		if (month == 0) { strcpy(dest, "January"); }
		else if (month == 1) { strcpy(dest, "February"); }
		else if (month == 2) { strcpy(dest, "March"); }
		else if (month == 3) { strcpy(dest, "April"); }
		else if (month == 4) { strcpy(dest, "May"); }
		else if (month == 5) { strcpy(dest, "June"); }
		else if (month == 6) { strcpy(dest, "July"); }
		else if (month == 7) { strcpy(dest, "August"); }
		else if (month == 8) { strcpy(dest, "September"); }
		else if (month == 9) { strcpy(dest, "October"); }
		else if (month == 10) { strcpy(dest, "November"); }
		else if (month == 11) { strcpy(dest, "December"); }
	}

}

void i18n_short_day(char *lang, int day, char *dest) {
	if (strcmp(lang, "da") == 0) {
		if (day == 1) { strcpy(dest, "Man"); }
		else if (day == 2) { strcpy(dest, "Tir"); }
		else if (day == 3) { strcpy(dest, "Ons"); }
		else if (day == 4) { strcpy(dest, "Tor"); }
		else if (day == 5) { strcpy(dest, "Fre"); }
		else if (day == 6) { strcpy(dest, "Lør"); }
		else if (day == 0) { strcpy(dest, "Søn"); }
	}
	else if (strcmp(lang, "de") == 0) {
		if (day == 1) { strcpy(dest, "Mon"); }
		else if (day == 2) { strcpy(dest, "ie"); }
		else if (day == 3) { strcpy(dest, "Mit"); }
		else if (day == 4) { strcpy(dest, "Don"); }
		else if (day == 5) { strcpy(dest, "Re"); }
		else if (day == 6) { strcpy(dest, "Sam"); }
		else if (day == 0) { strcpy(dest, "Son"); }
	}
	else if (strcmp(lang, "es") == 0) {
		if (day == 1) { strcpy(dest, "L"); }
		else if (day == 2) { strcpy(dest, "M"); }
		else if (day == 3) { strcpy(dest, "X"); }
		else if (day == 4) { strcpy(dest, "J"); }
		else if (day == 5) { strcpy(dest, "V"); }
		else if (day == 6) { strcpy(dest, "S"); }
		else if (day == 0) { strcpy(dest, "D"); }
	}
	else if (strcmp(lang, "fr") == 0) {
		if (day == 1) { strcpy(dest, "Lun"); }
		else if (day == 2) { strcpy(dest, "Mar"); }
		else if (day == 3) { strcpy(dest, "Mer"); }
		else if (day == 4) { strcpy(dest, "Jeu"); }
		else if (day == 5) { strcpy(dest, "Ven"); }
		else if (day == 6) { strcpy(dest, "Sam"); }
		else if (day == 0) { strcpy(dest, "Dim"); }
	}
	else if (strcmp(lang, "hr") == 0) {
		if (day == 1) { strcpy(dest, "Pon"); }
		else if (day == 2) { strcpy(dest, "Uto"); }
		else if (day == 3) { strcpy(dest, "Sri"); }
		else if (day == 4) { strcpy(dest, "Cet"); }
		else if (day == 5) { strcpy(dest, "Pet"); }
		else if (day == 6) { strcpy(dest, "Sub"); }
		else if (day == 0) { strcpy(dest, "Ned"); }
	}
	else if (strcmp(lang, "hu") == 0) {
		if (day == 1) { strcpy(dest, "Hét"); }
		else if (day == 2) { strcpy(dest, "Kedd"); }
		else if (day == 3) { strcpy(dest, "Sze"); }
		else if (day == 4) { strcpy(dest, "Csüt"); }
		else if (day == 5) { strcpy(dest, "Pén"); }
		else if (day == 6) { strcpy(dest, "Szo"); }
		else if (day == 0) { strcpy(dest, "Vas"); }
	}
	else if (strcmp(lang, "it") == 0) {
		if (day == 1) { strcpy(dest, "Lun"); }
		else if (day == 2) { strcpy(dest, "Mar"); }
		else if (day == 3) { strcpy(dest, "Mer"); }
		else if (day == 4) { strcpy(dest, "Gio"); }
		else if (day == 5) { strcpy(dest, "Ven"); }
		else if (day == 6) { strcpy(dest, "Sab"); }
		else if (day == 0) { strcpy(dest, "Dom"); }
	}
	else if (strcmp(lang, "ms") == 0) {
		if (day == 1) { strcpy(dest, "Isn"); }
		else if (day == 2) { strcpy(dest, "Sel"); }
		else if (day == 3) { strcpy(dest, "Rb"); }
		else if (day == 4) { strcpy(dest, "Kha"); }
		else if (day == 5) { strcpy(dest, "Jum"); }
		else if (day == 6) { strcpy(dest, "Sab"); }
		else if (day == 0) { strcpy(dest, "Ahd"); }
	}
	else if (strcmp(lang, "nl") == 0) {
		if (day == 1) { strcpy(dest, "Ma"); }
		else if (day == 2) { strcpy(dest, "Di"); }
		else if (day == 3) { strcpy(dest, "Wo"); }
		else if (day == 4) { strcpy(dest, "Do"); }
		else if (day == 5) { strcpy(dest, "Vr"); }
		else if (day == 6) { strcpy(dest, "Za"); }
		else if (day == 0) { strcpy(dest, "Zo"); }
	}
	else if (strcmp(lang, "ru") == 0) {
		if (day == 1) { strcpy(dest, "Пн"); }
		else if (day == 2) { strcpy(dest, "Вт"); }
		else if (day == 3) { strcpy(dest, "Ср"); }
		else if (day == 4) { strcpy(dest, "Чт"); }
		else if (day == 5) { strcpy(dest, "Пт"); }
		else if (day == 6) { strcpy(dest, "Сб"); }
		else if (day == 0) { strcpy(dest, "Вс"); }
	}
	else if (strcmp(lang, "sv") == 0) {
		if (day == 1) { strcpy(dest, "Mån"); }
		else if (day == 2) { strcpy(dest, "Tis"); }
		else if (day == 3) { strcpy(dest, "Ons"); }
		else if (day == 4) { strcpy(dest, "Tors"); }
		else if (day == 5) { strcpy(dest, "Fre"); }
		else if (day == 6) { strcpy(dest, "Lör"); }
		else if (day == 0) { strcpy(dest, "Sön"); }
	}
	else if (strcmp(lang, "tr") == 0) {
		if (day == 1) { strcpy(dest, "Pzt"); }
		else if (day == 2) { strcpy(dest, "Sal"); }
		else if (day == 3) { strcpy(dest, "Çar"); }
		else if (day == 4) { strcpy(dest, "Per"); }
		else if (day == 5) { strcpy(dest, "Cum"); }
		else if (day == 6) { strcpy(dest, "Cts"); }
		else if (day == 0) { strcpy(dest, "Paz"); }
	}
	else {
		if (day == 1) { strcpy(dest, "Mon"); }
		else if (day == 2) { strcpy(dest, "Tue"); }
		else if (day == 3) { strcpy(dest, "Wed"); }
		else if (day == 4) { strcpy(dest, "Thu"); }
		else if (day == 5) { strcpy(dest, "Fri"); }
		else if (day == 6) { strcpy(dest, "Sat"); }
		else if (day == 0) { strcpy(dest, "Sun"); }
	}

}

void i18n_day(char *lang, int day, char *dest) {
	if (strcmp(lang, "da") == 0) {
		if (day == 1) { strcpy(dest, "Mandag"); }
		else if (day == 2) { strcpy(dest, "Tirsdag"); }
		else if (day == 3) { strcpy(dest, "Onsdag"); }
		else if (day == 4) { strcpy(dest, "Torsdag"); }
		else if (day == 5) { strcpy(dest, "Fredag"); }
		else if (day == 6) { strcpy(dest, "Lørdag"); }
		else if (day == 0) { strcpy(dest, "Søndag"); }
	}
	else if (strcmp(lang, "de") == 0) {
		if (day == 1) { strcpy(dest, "Montag"); }
		else if (day == 2) { strcpy(dest, "Dienstag"); }
		else if (day == 3) { strcpy(dest, "Mittwoch"); }
		else if (day == 4) { strcpy(dest, "Donnerstag"); }
		else if (day == 5) { strcpy(dest, "Freitag"); }
		else if (day == 6) { strcpy(dest, "Samstag"); }
		else if (day == 0) { strcpy(dest, "Sonntag"); }
	}
	else if (strcmp(lang, "es") == 0) {
		if (day == 1) { strcpy(dest, "Lunes"); }
		else if (day == 2) { strcpy(dest, "Martes"); }
		else if (day == 3) { strcpy(dest, "Miércoles"); }
		else if (day == 4) { strcpy(dest, "Jueves"); }
		else if (day == 5) { strcpy(dest, "Viernes"); }
		else if (day == 6) { strcpy(dest, "Sábado"); }
		else if (day == 0) { strcpy(dest, "Domingo"); }
	}
	else if (strcmp(lang, "fr") == 0) {
		if (day == 1) { strcpy(dest, "Lundi"); }
		else if (day == 2) { strcpy(dest, "Mardi"); }
		else if (day == 3) { strcpy(dest, "Mercredi"); }
		else if (day == 4) { strcpy(dest, "Jeudi"); }
		else if (day == 5) { strcpy(dest, "Vendredi"); }
		else if (day == 6) { strcpy(dest, "Samedi"); }
		else if (day == 0) { strcpy(dest, "Dimanche"); }
	}
	else if (strcmp(lang, "hr") == 0) {
		if (day == 1) { strcpy(dest, "Ponedjeljak"); }
		else if (day == 2) { strcpy(dest, "Utorak"); }
		else if (day == 3) { strcpy(dest, "Srijeda"); }
		else if (day == 4) { strcpy(dest, "Cetvrtak"); }
		else if (day == 5) { strcpy(dest, "Petak"); }
		else if (day == 6) { strcpy(dest, "Subota"); }
		else if (day == 0) { strcpy(dest, "Nedjelja"); }
	}
	else if (strcmp(lang, "it") == 0) {
		if (day == 1) { strcpy(dest, "Lunedì"); }
		else if (day == 2) { strcpy(dest, "Martedì"); }
		else if (day == 3) { strcpy(dest, "Mercoledì"); }
		else if (day == 4) { strcpy(dest, "Giovedì"); }
		else if (day == 5) { strcpy(dest, "Venerdì"); }
		else if (day == 6) { strcpy(dest, "Sabato"); }
		else if (day == 0) { strcpy(dest, "Domenica"); }
	}
	else if (strcmp(lang, "la") == 0) {
		if (day == 1) { strcpy(dest, "Dies Lunae"); }
		else if (day == 2) { strcpy(dest, "Dies Mercurii"); }
		else if (day == 3) { strcpy(dest, "Dies Martis"); }
		else if (day == 4) { strcpy(dest, "Dies Lovis"); }
		else if (day == 5) { strcpy(dest, "Dies Venerius"); }
		else if (day == 6) { strcpy(dest, "Dies Saturnis"); }
		else if (day == 0) { strcpy(dest, "Dies Solis"); }
	}
	else if (strcmp(lang, "ms") == 0) {
		if (day == 1) { strcpy(dest, "Isnin"); }
		else if (day == 2) { strcpy(dest, "Selasa"); }
		else if (day == 3) { strcpy(dest, "Rabu"); }
		else if (day == 4) { strcpy(dest, "Khamis"); }
		else if (day == 5) { strcpy(dest, "Jumaat"); }
		else if (day == 6) { strcpy(dest, "Sabtu"); }
		else if (day == 0) { strcpy(dest, "Ahad"); }
	}
	else if (strcmp(lang, "nl") == 0) {
		if (day == 1) { strcpy(dest, "Maandag"); }
		else if (day == 2) { strcpy(dest, "Dinsdag"); }
		else if (day == 3) { strcpy(dest, "Woensdag"); }
		else if (day == 4) { strcpy(dest, "Donderdag"); }
		else if (day == 5) { strcpy(dest, "Vrijdag"); }
		else if (day == 6) { strcpy(dest, "Zaterdag"); }
		else if (day == 0) { strcpy(dest, "Zondag"); }
	}
	else if (strcmp(lang, "ru") == 0) {
		if (day == 1) { strcpy(dest, "Понедельник"); }
		else if (day == 2) { strcpy(dest, "Вторник"); }
		else if (day == 3) { strcpy(dest, "Среда"); }
		else if (day == 4) { strcpy(dest, "Четверг"); }
		else if (day == 5) { strcpy(dest, "Пятница"); }
		else if (day == 6) { strcpy(dest, "Суббота"); }
		else if (day == 0) { strcpy(dest, "Воскресенье"); }
	}
	else if (strcmp(lang, "sv") == 0) {
		if (day == 1) { strcpy(dest, "Måndag"); }
		else if (day == 2) { strcpy(dest, "Tisdag"); }
		else if (day == 3) { strcpy(dest, "Onsdag"); }
		else if (day == 4) { strcpy(dest, "Torsdag"); }
		else if (day == 5) { strcpy(dest, "Fredag"); }
		else if (day == 6) { strcpy(dest, "Lördag"); }
		else if (day == 0) { strcpy(dest, "Söndag"); }
	}
	else if (strcmp(lang, "tr") == 0) {
		if (day == 1) { strcpy(dest, "Pazartesi"); }
		else if (day == 2) { strcpy(dest, "Salı"); }
		else if (day == 3) { strcpy(dest, "Çarşamba"); }
		else if (day == 4) { strcpy(dest, "Perşembe"); }
		else if (day == 5) { strcpy(dest, "Cuma"); }
		else if (day == 6) { strcpy(dest, "Cumartesi"); }
		else if (day == 0) { strcpy(dest, "Pazar"); }
	}
	else {
		if (day == 1) { strcpy(dest, "Monday"); }
		else if (day == 2) { strcpy(dest, "Tuesday"); }
		else if (day == 3) { strcpy(dest, "Wednesday"); }
		else if (day == 4) { strcpy(dest, "Thursday"); }
		else if (day == 5) { strcpy(dest, "Friday"); }
		else if (day == 6) { strcpy(dest, "Saturday"); }
		else if (day == 0) { strcpy(dest, "Sunday"); }
	}

}

void i18n_meridiem(char *lang, int hour, char *dest) {
	if (strcmp(lang, "hu") == 0) {
		if (hour < 12) { strcpy(dest, "DE"); }
		else { strcpy(dest, "DU"); }
	}
	else if (strcmp(lang, "ms") == 0) {
		if (hour < 12) { strcpy(dest, "PG"); }
		else { strcpy(dest, "PT"); }
	}
	else if (strcmp(lang, "ru") == 0) {
		if (hour < 12) { strcpy(dest, "ДП"); }
		else { strcpy(dest, "ПП"); }
	}
	else {
		if (hour < 12) { strcpy(dest, "AM"); }
		else { strcpy(dest, "PM"); }
	}

}

void i18n_time_period(char *lang, char *src, int num, char *dest) {
		int value = (num != 1) ? 1 : 0;
		if (strcmp(src, "Second") == 0) {
			if (value == 0) { strcpy(dest, "Second"); }
			else if (value == 1) { strcpy(dest, "Seconds"); }
		}
		else if (strcmp(src, "Minute") == 0) {
			if (value == 0) { strcpy(dest, "Minute"); }
			else if (value == 1) { strcpy(dest, "Minutes"); }
		}
		else if (strcmp(src, "Hour") == 0) {
			if (value == 0) { strcpy(dest, "Hour"); }
			else if (value == 1) { strcpy(dest, "Hours"); }
		}
		else if (strcmp(src, "Day") == 0) {
			if (value == 0) { strcpy(dest, "Day"); }
			else if (value == 1) { strcpy(dest, "Days"); }
		}
		else if (strcmp(src, "Week") == 0) {
			if (value == 0) { strcpy(dest, "Week"); }
			else if (value == 1) { strcpy(dest, "Weeks"); }
		}
		else if (strcmp(src, "Month") == 0) {
			if (value == 0) { strcpy(dest, "Month"); }
			else if (value == 1) { strcpy(dest, "Months"); }
		}
		else if (strcmp(src, "Year") == 0) {
			if (value == 0) { strcpy(dest, "Year"); }
			else if (value == 1) { strcpy(dest, "Years"); }
		}

}

void i18n_weather(char *lang, char *src, char *dest) {
		if (strcmp(src, "Clear") == 0) { strcpy(dest, "Clear"); }
		else if (strcmp(src, "Sunny") == 0) { strcpy(dest, "Sunny"); }
		else if (strcmp(src, "Cloudy") == 0) { strcpy(dest, "Cloudy"); }
		else if (strcmp(src, "Partly Cloudy") == 0) { strcpy(dest, "Partly Cloudy"); }
		else if (strcmp(src, "Mist") == 0) { strcpy(dest, "Mist"); }
		else if (strcmp(src, "Fog") == 0) { strcpy(dest, "Fog"); }
		else if (strcmp(src, "Light Rain") == 0) { strcpy(dest, "Light Rain"); }
		else if (strcmp(src, "Rain") == 0) { strcpy(dest, "Rain"); }
		else if (strcmp(src, "Heavy Rain") == 0) { strcpy(dest, "Heavy Rain"); }
		else if (strcmp(src, "Thunderstorm") == 0) { strcpy(dest, "Thunderstorm"); }
		else if (strcmp(src, "Snow") == 0) { strcpy(dest, "Snow"); }
		else if (strcmp(src, "Hail") == 0) { strcpy(dest, "Hail"); }
		else if (strcmp(src, "Sleet") == 0) { strcpy(dest, "Sleet"); }
		else if (strcmp(src, "Windy") == 0) { strcpy(dest, "Windy"); }
		else if (strcmp(src, "Wind") == 0) { strcpy(dest, "Wind"); }
		else if (strcmp(src, "Extreme Wind") == 0) { strcpy(dest, "Extreme Wind"); }
		else if (strcmp(src, "Tornado") == 0) { strcpy(dest, "Tornado"); }
		else if (strcmp(src, "Cold") == 0) { strcpy(dest, "Cold"); }
		else if (strcmp(src, "Hot") == 0) { strcpy(dest, "Hot"); }
		else if (strcmp(src, "Extreme Cold") == 0) { strcpy(dest, "Extreme Cold"); }
		else if (strcmp(src, "Extreme Heat") == 0) { strcpy(dest, "Extreme Heat"); }
		else if (strcmp(src, "Overcast") == 0) { strcpy(dest, "Overcast"); }
		else if (strcmp(src, "Haze") == 0) { strcpy(dest, "Haze"); }
		else if (strcmp(src, "Sprinkle") == 0) { strcpy(dest, "Sprinkle"); }
		else if (strcmp(src, "Lightning") == 0) { strcpy(dest, "Lightning"); }

}

void i18n_generic(char *lang, char *src, char *dest) {
		if (strcmp(src, "Ok") == 0) { strcpy(dest, "Ok"); }
		else if (strcmp(src, "Error") == 0) { strcpy(dest, "Error"); }

}

void i18n_device(char *lang, char *src, char *dest) {
		if (strcmp(src, "Battery") == 0) { strcpy(dest, "Battery"); }
		else if (strcmp(src, "Charging") == 0) { strcpy(dest, "Charging"); }
		else if (strcmp(src, "Discharging") == 0) { strcpy(dest, "Discharging"); }
		else if (strcmp(src, "Full") == 0) { strcpy(dest, "Full"); }
		else if (strcmp(src, "Empty") == 0) { strcpy(dest, "Empty"); }
		else if (strcmp(src, "Bluetooth") == 0) { strcpy(dest, "Bluetooth"); }
		else if (strcmp(src, "Connect") == 0) { strcpy(dest, "Connect"); }
		else if (strcmp(src, "Disconnect") == 0) { strcpy(dest, "Disconnect"); }
		else if (strcmp(src, "Connected") == 0) { strcpy(dest, "Connected"); }
		else if (strcmp(src, "Disconnected") == 0) { strcpy(dest, "Disconnected"); }

}
