#include <string.h>

void i18n_short_month(char *lang, int month, char *dest) {
<%= i18n_short_month %>
}

void i18n_month(char *lang, int month, char *dest) {
<%= i18n_month %>
}

void i18n_short_day(char *lang, int day, char *dest) {
<%= i18n_short_day %>
}

void i18n_day(char *lang, int day, char *dest) {
<%= i18n_day %>
}

void i18n_meridiem(char *lang, int hour, char *dest) {
<%= i18n_meridiem %>
}

void i18n_time_period(char *lang, char *src, int num, char *dest) {
<%= i18n_time_period %>
}

void i18n_weather(char *lang, char *src, char *dest) {
<%= i18n_weather %>
}

void i18n_generic(char *lang, char *src, char *dest) {
<%= i18n_generic %>
}

void i18n_device(char *lang, char *src, char *dest) {
<%= i18n_device %>
}
