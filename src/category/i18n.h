#pragma once

#include <pebble.h>

void i18n_short_month(char *lang, int month, char *dest);
void i18n_month(char *lang, int month, char *dest);
void i18n_short_day(char *lang, int day, char *dest);
void i18n_day(char *lang, int day, char *dest);
void i18n_meridiem(char *lang, int hour, char *dest);
void i18n_time_period(char *lang, char *src, int num, char *dest);
void i18n_weather(char *lang, char *src, char *dest);
void i18n_generic(char *lang, char *src, char *dest);
void i18n_device(char *lang, char *src, char *dest);
