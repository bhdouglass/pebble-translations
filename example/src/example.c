#include <pebble.h>

#include "i18n.h"

static Window *window;
static TextLayer *text_layer;
GFont font;

char month[20];
char lang[3] = "ru";

static void window_load(Window *window) {
    i18n_month(lang, 1, month);
    APP_LOG(APP_LOG_LEVEL_DEBUG, month);

    Layer *window_layer = window_get_root_layer(window);
    GRect bounds = layer_get_bounds(window_layer);

    text_layer = text_layer_create(bounds);
    text_layer_set_font(text_layer, font);
    text_layer_set_text(text_layer, month);
    text_layer_set_text_alignment(text_layer, GTextAlignmentCenter);
    layer_add_child(window_layer, text_layer_get_layer(text_layer));
}

static void window_unload(Window *window) {
    text_layer_destroy(text_layer);
}

static void init(void) {
    font = fonts_load_custom_font(resource_get_handle(RESOURCE_ID_DROIDSANS_16));

    window = window_create();
    window_set_window_handlers(window, (WindowHandlers) {
        .load = window_load,
        .unload = window_unload,
    });

    const bool animated = true;
    window_stack_push(window, animated);
}

static void deinit(void) {
    window_destroy(window);
    fonts_unload_custom_font(font);
}

int main(void) {
    init();

    app_event_loop();
    deinit();
}
