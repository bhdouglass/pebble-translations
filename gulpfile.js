var gulp = require('gulp');
var change = require('gulp-change');
var rename = require('gulp-rename');
var concat = require('gulp-concat');
var template = require('gulp-template');
var shell = require('gulp-shell');
var minimist = require('minimist');
var pofile = require('pofile');
var del = require('del');
var fs = require('fs');
var _ = require('lodash');
var merge = require('merge-stream');

gulp.task('clean', function() {
    del.sync('dist');
});

var puralReplacements = {
    '(n != 1)': '(n != 1) ? 1 : 0',
    '(n > 1)': '(n > 1) ? 1 : 0',
};

var config = minimist(process.argv.slice(2), {
    default: {
        emulator: false,
        color: false,
        ip: '192.168.1.21',
        logs: false,
        debug: false,
        config: 'http://pebble-translate-example.bhdouglass.com/',
    },
    boolean: ['emulator', 'color'],
    alias: {
        emulator: ['e', 'emu'],
        color: 'c',
        logs: 'l',
        debug: 'd',
    }
});

function installCommand(config) {
    var command = 'pebble install';
    if (config.emulator) {
        command += ' --emulator';

        if (config.color) {
            command += ' basalt';
        }
        else {
            command += ' aplite';
        }
    }
    else {
        command += ' --phone ' + config.ip;
    }

    if (config.logs) {
        command += ' --logs';
    }

    if (config.debug) {
        command += ' --debug';
    }

    return command;
}

function parseCategoryPO(content, useMsgid) {
    var po = pofile.parse(content);

    var plurals = po.headers['Plural-Forms'].substr(19).replace(';', '');
    if (useMsgid) {
        plurals = '(n != 1) ? 1 : 0';
    }
    else {
        if (puralReplacements[plurals]) {
            plurals = puralReplacements[plurals];
        }
    }

    var json = {
        code: po.headers.Language,
        name: po.headers['Language-Team'].substr(0, po.headers['Language-Team'].indexOf('(') - 1),
        plurals: plurals,
        incomplete: [],
        characters: '',
    };

    po.items.forEach(function(item) {
        var key = item.extractedComments[0].toLowerCase().replace(/ /g, '_');
        if (key.indexOf('-') > -1) {
            key = key.substr(0, key.indexOf('-') - 1);
        }

        if (!json[key]) {
            json[key] = [];
        }

        if (useMsgid) {
            json.characters += item.msgid;
            if (item.msgid_plural) {
                json[key].push([item.msgid, item.msgid_plural]);
                json.characters += item.msgid_plural;
            }
            else {
                json[key].push(item.msgid);
            }
        }
        else {
            item.msgstr.forEach(function(str) {
                json.characters += str;
            });

            if (item.msgstr.length == 1) {
                json[key].push(item.msgstr[0]);
            }
            else {
                json[key].push(item.msgstr);
            }

            if (item.msgstr[0].length === 0) {
                if (json.incomplete.indexOf(key) == -1) {
                    json.incomplete.push(key);
                }
            }
        }
    });

    var uniqChars = _.sortBy(_.uniq(json.characters.split('')));
    json.characters = uniqChars.join('');

    return json;
}

gulp.task('build-category-json', function() {
    return gulp.src('po/*.po')
        .pipe(change(function(content) {
            var json = parseCategoryPO(content);
            return JSON.stringify(json, null, 4);
        }))
        .pipe(rename({
            extname: '.json'
        }))
        .pipe(gulp.dest('dist/json/category'));
});

gulp.task('build-all-category-json', ['build-category-json'], function() {
    return gulp.src(['dist/json/category/*.json', '!dist/json/category/pebble-all.json'])
        .pipe(change(function(content) {
            return content + ',';
        }))
        .pipe(concat('pebble-all.json'))
        .pipe(change(function(content) {
            content = '[' + content.substr(0, content.length - 1) + ']';
            var json = JSON.parse(content);
            var newJson = {};
            json.forEach(function(language) {
                newJson[language.code] = language;
            });

            return JSON.stringify(newJson, null, 4);
        }))
        .pipe(gulp.dest('dist/json/category'));
});

//TODO handle plurals
gulp.task('build-key-value-json', function() {
    return gulp.src('po/*.po')
        .pipe(change(function(content) {
            var po = pofile.parse(content);

            var json = {};
            po.items.forEach(function(item) {
                if (item.msgstr.length == 1) {
                    json[item.msgid] = item.msgstr[0];
                }
                else {
                    json[item.msgid] = item.msgstr;
                }
            });

            return JSON.stringify(json, null, 4);
        }))
        .pipe(rename({
            extname: '.json'
        }))
        .pipe(gulp.dest('dist/json/keyvalue'));
});

function languageHeader(src, code, isFallback) {
    var end = true;
    if (isFallback) {
        if (src.length === 0) {
            end = false;
        }
        else {
            src += '\telse {\n';
        }
    }
    else {
        if (src.length === 0) {
            src += '\tif (strcmp(lang, "' + code + '") == 0) {\n';
        }
        else {
            src += '\telse if (strcmp(lang, "' + code + '") == 0) {\n';
        }
    }

    return {
        end: end,
        src: src,
    };
}

function buildCTime(src, code, translations, day, isFallback) {
    var index = 0;
    if (day) {
        index = 1;
    }

    var header = languageHeader(src, code, isFallback);
    src = header.src;

    translations.forEach(function(str) {
        var time = day ? 'day' : 'month';
        if ((index != 1 && day) || (index != 0 && !day)) {
            src += '\t\telse if (' + time + ' == ' + index + ') { strcpy(dest, "' + str + '"); }\n';
        }
        else {
            src += '\t\tif (' + time + ' == ' + index + ') { strcpy(dest, "' + str + '"); }\n';
        }

        index++;
        if (index == 7 && day) {
            index = 0;
        }
    });

    if (header.end) {
        src += '\t}\n';
    }

    return src;
}

function buildCMeridiem(src, code, translations, isFallback) {
    var header = languageHeader(src, code, isFallback);
    src = header.src;

    translations.forEach(function(str, index) {
        if (index == 0) {
            src += '\t\tif (hour < 12) { strcpy(dest, "' + str + '"); }\n';
        }
        else {
            src += '\t\telse { strcpy(dest, "' + str + '"); }\n';
        }
    });

    if (header.end) {
        src += '\t}\n';
    }

    return src;
}

function buildCLang(src, code, translations, plurals, fallback, isFallback, usePlurals) {
    var header = languageHeader(src, code, isFallback);
    src = header.src;

    if (usePlurals) {
        src += '\t\tint value = ' + plurals.replace(/n/g, 'num') + ';\n';
    }

    //TODO handle plurals
    translations.forEach(function(str, index) {
        var fb = fallback[index];
        if (Array.isArray(fb)) {
            fb = fb[0];
        }

        if (index == 0) {
            if (Array.isArray(str)) {
                src += '\t\tif (strcmp(src, "' + fb + '") == 0) {\n';
                    str.forEach(function(substr, index) {
                        if (index === 0) {
                            src += '\t\t\tif (value == ' + index + ') { strcpy(dest, "' + substr + '"); }\n';
                        }
                        else {
                            src += '\t\t\telse if (value == ' + index + ') { strcpy(dest, "' + substr + '"); }\n';
                        }
                    });
                src += '\t\t}\n';
            }
            else {
                src += '\t\tif (strcmp(src, "' + fb + '") == 0) { strcpy(dest, "' + str + '"); }\n';
            }
        }
        else {
            if (Array.isArray(str)) {
                src += '\t\telse if (strcmp(src, "' + fb + '") == 0) {\n';
                    str.forEach(function(substr, index) {
                        if (index === 0) {
                            src += '\t\t\tif (value == ' + index + ') { strcpy(dest, "' + substr + '"); }\n';
                        }
                        else {
                            src += '\t\t\telse if (value == ' + index + ') { strcpy(dest, "' + substr + '"); }\n';
                        }
                    });
                src += '\t\t}\n';
            }
            else {
                src += '\t\telse if (strcmp(src, "' + fb + '") == 0) { strcpy(dest, "' + str + '"); }\n';
            }
        }
    });

    if (header.end) {
        src += '\t}\n';
    }

    return src;
}

gulp.task('build-category-c', ['build-all-category-json'], function() {
    var languages = fs.readFileSync('dist/json/category/pebble-all.json', 'utf8');
    languages = JSON.parse(languages);

    var potfile = fs.readFileSync('po/pebble.pot', 'utf8');
    var fallback = parseCategoryPO(potfile, true);

    var i18n_short_month = '';
    var i18n_month = '';
    var i18n_short_day = '';
    var i18n_day = '';
    var i18n_meridiem = '';
    var i18n_time_period = '';
    var i18n_weather = '';
    var i18n_generic = '';
    var i18n_device = '';

    for (var code in languages) {
        var language = languages[code];

        if (language.incomplete.indexOf('short_month') == -1) {
            i18n_short_month = buildCTime(i18n_short_month, code, language.short_month, false);
        }

        if (language.incomplete.indexOf('month') == -1) {
            i18n_month = buildCTime(i18n_month, code, language.month, false);
        }

        if (language.incomplete.indexOf('short_day') == -1) {
            i18n_short_day = buildCTime(i18n_short_day, code, language.short_day, true);
        }

        if (language.incomplete.indexOf('day') == -1) {
            i18n_day = buildCTime(i18n_day, code, language.day, true);
        }

        if (language.incomplete.indexOf('meridiem') == -1) {
            i18n_meridiem = buildCMeridiem(i18n_meridiem, code, language.meridiem);
        }

        if (language.incomplete.indexOf('time_period') == -1) {
            i18n_time_period = buildCLang(i18n_time_period, code, language.time_period, language.plurals, fallback.time_period, false, true);
        }

        if (language.incomplete.indexOf('weather') == -1) {
            i18n_weather = buildCLang(i18n_weather, code, language.weather, language.plurals, fallback.weather);
        }

        if (language.incomplete.indexOf('generic') == -1) {
            i18n_generic = buildCLang(i18n_generic, code, language.generic, language.plurals, fallback.generic);
        }

        if (language.incomplete.indexOf('device') == -1) {
            i18n_device = buildCLang(i18n_device, code, language.device, language.plurals, fallback.device);
        }
    }

    i18n_short_month = buildCTime(i18n_short_month, null, fallback.short_month, false, true);
    i18n_month = buildCTime(i18n_month, null, fallback.month, false, true);
    i18n_short_day = buildCTime(i18n_short_day, null, fallback.short_day, true, true);
    i18n_day = buildCTime(i18n_day, null, fallback.day, true, true);
    i18n_meridiem = buildCMeridiem(i18n_meridiem, null, fallback.meridiem, true);
    i18n_time_period = buildCLang(i18n_time_period, null, fallback.time_period, fallback.plurals, fallback.time_period, true, true);
    i18n_weather = buildCLang(i18n_weather, null, fallback.weather, fallback.plurals, fallback.weather, true);
    i18n_generic = buildCLang(i18n_generic, null, fallback.generic, fallback.plurals, fallback.generic, true);
    i18n_device = buildCLang(i18n_device, null, fallback.device, fallback.plurals, fallback.device, true);

    return gulp.src('src/category/*')
        .pipe(template({
            i18n_short_month: i18n_short_month,
            i18n_month: i18n_month,
            i18n_short_day: i18n_short_day,
            i18n_day: i18n_day,
            i18n_meridiem: i18n_meridiem,
            i18n_time_period: i18n_time_period,
            i18n_weather: i18n_weather,
            i18n_generic: i18n_generic,
            i18n_device: i18n_device,
        }))
        .pipe(gulp.dest('dist/c/category'));
});

gulp.task('build', ['clean', 'build-category-c', 'build-key-value-json']);

gulp.task('clean', function() {
    del.sync('example/dist');
});

gulp.task('move-example', ['build'], function() {
    var languages = fs.readFileSync('dist/json/category/pebble-all.json', 'utf8');
    languages = JSON.parse(languages);

    var character_regex = '';
    for (var code in languages) {
        character_regex += languages[code].characters;
    }

    var uniqChars = _.sortBy(_.uniq(character_regex.split('')));
    character_regex = '[' + uniqChars.join('') + ']';

    return merge(
        gulp.src(['example/appinfo.json'])
            .pipe(template({
                character_regex: character_regex,
            })),

        gulp.src(['example/**/*', '!example/dist/**/*', '!example/appinfo.json']),

        gulp.src(['dist/c/category/*'])
            .pipe(gulp.dest('example/dist/src'))
    ).pipe(gulp.dest('example/dist'));
});

gulp.task('build-example', ['clean', 'move-example'], shell.task(['cd example/dist && pebble build']));

gulp.task('install-example', ['build-example'], shell.task(['cd example/dist && ' + installCommand(config)]));
