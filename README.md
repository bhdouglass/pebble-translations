# Pebble Translations
Common translations for Pebble watch apps including Date, Time, Weather, and Device strings.

Translated via [Transifex](https://www.transifex.com/brian-douglass/pebble-translations/).

## Suggest New Strings

Suggest new strings by submitting an issue on this [Github page](https://github.com/bhdouglass/pebble-translations).

## How to Translate

1) Go to the [translation project on Transifex](https://www.transifex.com/brian-douglass/pebble-translations/)
and click the "Help Translate" button.
![Step 1](images/step1.png)

2) Sign up for a Transifex account.
![Step 2](images/step2.png)

3) Click on the language you would like to translate
![Step 3](images/step3.png)

4) Click on "pebble.pot"
![Step 4](images/step4.png)

5) Click on the "Translate" button
![Step 5](images/step5.png)

6) Start translating!
![Step 6](images/step6.png)

## Missing Languages in Transifex

Request new languages by submitting an issue on this [Github page](https://github.com/bhdouglass/pebble-translations).

## License

Copyright (c) 2015 Brian Douglass

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
